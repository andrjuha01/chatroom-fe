import { writable } from 'svelte/store';

let loggedInUser
const user = writable(null);
try {
    loggedInUser = JSON.parse(localStorage.getItem("user"))
    if (loggedInUser) {
        user.set(loggedInUser)
    }
} catch(e) {
    console.log(e)
}

export default user
